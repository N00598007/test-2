﻿<%@ Page Language="C#" Inherits="Assignment1_A.Default" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>Default</title>
</head>
<body>
	 <form id="form1" runat="server">   
        <table style="border: 1px solid black">
            <tr>
                <td colspan="2">
                    <h1>Mailing Info</h1>   
                </td>
            </tr>
            <tr>
                <td>
                    <b>First Name:</b>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="clientFName" placeholder="First Name" Width="100"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your first name" ControlToValidate="clientFName" ID="validatorfname" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Last Name:</b>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="clientLName" placeholder="Last Name" Width="100"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your last name" ControlToValidate="clientLName" ID="validatorlname" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                    <td>
                    <b>Address:</b>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="address" placeholder="Address"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your address" ControlToValidate="address" ID="validatorAddress" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <b>City:</b>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="city" placeholder="City"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your city" ControlToValidate="city" ID="validatorCity" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Province:</b>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="prov" placeholder="Province"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your province" ControlToValidate="prov" ID="validatorProv" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>   
            <tr>
                <td>
                    <b>Postal Code:</b>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="postcode" placeholder="Postal Code"></asp:TextBox>        
                    <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "postcode" ID="RegularExpressionValidator1" ValidationExpression = "(\D{1}\d{1}\D{1}\-?\d{1}\D{1}\d{1})|(\D{1}\d{1}\D{1}\ ?\d{1}\D{1}\d{1})" 
                                                    runat="server" ErrorMessage="Please Enter Valid Postal Code" ForeColor="Red"></asp:RegularExpressionValidator>
                
                </td>
            </tr>      
            <tr>
                <td colspan="2">
                    <h1>Billing Info</h1>        
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button runat="server" ID="myButton" Text="Submit"/>
                </td>
            </tr>               
        </table>
    </form>
</body>
</html>
